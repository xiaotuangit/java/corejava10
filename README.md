# corejava10

Java核心技术 卷I 基础知识（原书第10版）

第2章 Java程序设计环境

	1. Welcome					    	===> 2.2 使用命令行工具 -- 向控制台输出一条消息
	2. ImageViewer					    ===> 2.4 运行图形化应用程序 -- 该程序用于显示一张图片
	3. RoadApplet						===> 2.5 构建并运行applet -- Applet应用程序，这个程序显示了司机随意减速可能导致交通拥堵的情况
	
第3章 Java的基本程序设计结构

	1. FirstSample						===> 3.2 注释 -- 文档注释的使用示例程序
	2. InputTest						===> 3.7.1 读取输入 -- 演示使用Scanner类进行控制台输入
	3. Retirement						===> 3.8.3 循环 -- 使用while循环（计算需要多长时间才能够存储一定数量的退休金）
	4. Retirement2						===> 3.8.3 循环 -- 使用do while循环（计算需要多长时间才能够存储一定数量的退休金）
	5. LotteryOdds						===> 3.8.4 确定循环 -- 计算抽奖中奖的概率
	6. BigIntegerTest					===> 3.9 大数值 -- 使用大数值计算中彩概率
	7. LotteryDrawing					===> 3.10.5 数组排序 -- 排序输出数组
	8. CompoundInterest					===> 3.10.6 多维数组 -- 打印多维数组
	9. LotteryArray						===> 3.10.7 不规则数组 -- 不规则数组的使用

第4章 对象与类

	1. CalendarTest						===> 4.2.3 更改器方法与访问器方法 -- 显示当前月份的日历，突显今日日期
	2. EmployeeTest						===> 4.3 用户自定义类 -- Employee类
	3. StaticTest						===> 4.4.5 main方法 -- 静态方法的使用
	4. ParamTest						===> 4.5 方法参数 -- 演示Java中的参数传递
	5. ConstructorTest					===> 4.6.7 初始化块 -- 对象的构造方法
	6. PackageTest						===> 4.7.3 将类放入包 -- 使用包

第5章 继承

	1. inheritance						===> 5.1.3 子类构造器 -- Employee对象与Manager对象在薪水计算上的区别
	2. AbstractClasses					===> 5.1.8 强制类型转换 -- 抽象类使用
	3. Equals							===> 5.2.4 toString方法 -- 实现Employee类和Manager类的equals、hashCode和toString方法
	4. ArrayList						===> 5.3.1 访问数组列表元素 -- ArrayList的使用
	5. Enums							===> 5.6 枚举类 -- 枚举类的工作方式
	6. Reflection						===> 5.7.3 利用反射分析类的能力 -- 打印一个类的全部信息的方法
	7. ObjectAnalyzer					===> 5.7.4 在运行时使用反射分析对象 -- 一个可供任意类使用的通用toString方法
	8 Arrays							===> 5.7.5 使用反射编写泛型数组代码 -- 两个扩展数组的方法
	9. Methods 							===> 5.7.6 调用任意方法 -- 通用制表和两个厕所程序