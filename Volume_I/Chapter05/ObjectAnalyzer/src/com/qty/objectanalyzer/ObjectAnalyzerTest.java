package com.qty.objectanalyzer;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This program uses reflection to spy on objects.
 * @version 1.12 2012-01-26
 * @author Xiaotuan
 */
public class ObjectAnalyzerTest {

	public static void main(String[] args) {
		ArrayList<Integer> squares = new ArrayList<>();
		for (int i = 1; i <= 5; i++) {
			squares.add(i * i);
		}
		System.out.println(Arrays.toString(squares.toArray()));
		System.out.println(new ObjectAnalyzer().toString(squares));
	}
}
